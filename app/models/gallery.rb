class Gallery < ActiveRecord::Base
	attachment :image

	validates :image, presence: true

  scope :default, -> {order('created_at desc').limit(20)}

end
