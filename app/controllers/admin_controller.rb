class AdminController < ApplicationController
	before_action :authenticate_user!
  def index
  	@body_class = 'admin'
  	@fotos = Gallery.count
  end
end
