json.array!(@galleries) do |gallery|
  json.extract! gallery, :id, :desc, :image_id
  json.url gallery_url(gallery, format: :json)
end
