class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.text :desc
      t.string :image_id

      t.timestamps null: false
    end
  end
end
